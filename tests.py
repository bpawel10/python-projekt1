import pytest
import main as m

def test1():
    assert m.minimize('1|0') == '1'

def test2():
    assert m.minimize('1&1') == '1'

def test3():
    assert m.minimize('~a | a') == '1'

def test4():
    assert m.minimize('~abc & ~def & ghi | abc & ~def&  ghi') == '~def&ghi'

def test5():
    assert m.minimize('~a & ~b & ~c | ~a & ~b & c | a & ~b & c | a & b & c | a & b & ~c') == '~b&c|~a&~b|a&b'

def test6():
    assert m.minimize('a|~b & ~c & d| ~d ^a & b^ ~d | c') == 'a|c|~b&d'

def test7():
    assert m.minimize('a& b| c &d & ~a| ~a') == '~a|b'

def test8():
    assert m.minimize('~a & ~a') == '~a'

def test9():
    assert m.minimize('a | ~a | b | ~b') == '1'

def test10():
    assert m.minimize('a | ~a & b | ~b & c & ~a') == 'a|b|c'
