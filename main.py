import re
import sys
from pyeda.inter import *
import config

def parse(string):
    string = string.replace(' ', '')
    pattern = '('+'|'.join(list(config.OPERATORS.keys()))+')\(([a-z0-9,'+''.join(list(config.OPERATORS.values()))+']+)\)'

    for str in re.finditer(pattern, string):
        old = str.group()

        for key, value in config.OPERATORS.items():
            if old.startswith(key):
                new = old[len(key)+1:-1].replace(',', value)
                break

        string = string.replace(old, new)

    if re.search(pattern, string):
        return parse(string)
    else:
        return string

def minimize(string):
    exp = expr(string)

    if exp.is_dnf():
        exp = parse(str(espresso_exprs(exp))[1:-2])

    return str(exp)

def main():
    if len(sys.argv[1:]) != 1:
        print('Wrong number of arguments')
    else:
        print(minimize(sys.argv[1:][0]))

    sys.exit()

if __name__ == "__main__":
    main()
