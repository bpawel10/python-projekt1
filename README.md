# Python - projekt 1 - minimalizacja wyrażenia logicznego

## Wymagania
Aby uruchomić program, należy zainstalować moduł [pyeda](https://pypi.org/project/pyeda), który dostarcza gotową implementację [metody Espresso minimalizacji funkcji logicznych](https://en.wikipedia.org/wiki/Espresso_heuristic_logic_minimizer):
```
pip3 install pyeda
```

## Uruchomienie
Wyrażenie logiczne do minimalizacji należy podać jako argument, na przykład:
```
python3 main.py "~abc & ~def & ghi | abc & ~def&  ghi"
```
Program wypisze zminimalizowane wyrażenie, a następnie zakończy działanie.

## Testy
Aby uruchomić testy, należy zainstalować moduł [pytest](https://docs.pytest.org/en/latest) i wywołać:
```
pytest tests.py
```

## Błędy
Program działa bardzo dobrze z operatorami negacji `~`, alternatywy `|` i koniunkcji `&` oraz zmiennymi alfanumerycznymi, ale czasami nie radzi sobie z obsługą stałych logicznych (np. `0 & 1`) lub operatora XOR `^`. Operatory implikacji `>` i równoważności `=` także powodują błędy.
